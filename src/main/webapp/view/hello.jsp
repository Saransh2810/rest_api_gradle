<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <style><%@include file="/stylesheet/style.css"%></style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>


<body>
    <table class="table table-bordered table-striped" id="geotable">
        <tr>
            <th>Id</th>
            <th>Zip</th>
            <th>City</th>
            <th>State</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th>Timezone</th>
            <th>DST</th>
        </tr>
        <c:forEach var="j" begin="0" end="43204">

                <tr>
                    <td>${task[j].id}</td>
                     <td>${task[j].zip}</td>
                      <td>${task[j].city}</td>
                       <td>${task[j].state}</td>
                       <td>${task[j].latitude}</td>
                       <td>${task[j].longitude}</td>
                       <td>${task[j].timezone}</td>
                       <td>${task[j].dst}</td>


                </tr>
            </c:forEach>
    </table>

</body>

</html>

