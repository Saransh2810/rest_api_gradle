<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <form action="addData" method="POST">
          <div class="container">
            <h1>Student Register</h1>

            <hr>



                        <label><b>Name</b></label>
                        <input type="text" placeholder="Enter your name" name="name" id="name" required><hr>

                        <label><b>Age</b></label>
                        <input type="number" placeholder="Enter your age" name="age" id="age" required>
                        <hr>

                        <label><b>City</b></label>
                        <input type="text" placeholder="Enter your city" name="city" id="city" required>
                        <hr>

                        <label><b>Contact No.</b></label>
                        <input type="text" placeholder="Enter your contact number" name="contact_number" id="cn" required>
                        <hr>

                        <button type="submit" class="registerbtn" onclick="getDetails()">Save</button>
          </div>
        </form>
        <form action="tas">
            <button type="submit" class="getall" >Get All</button>
        </form>

    </body>
</html>