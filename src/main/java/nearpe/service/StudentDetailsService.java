package nearpe.service;

import nearpe.model.AreaInfo;
import nearpe.model.StudentDetails;
import nearpe.repository.StudentDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentDetailsService {

    @Autowired
    StudentDetailRepository studentDetailsRepository;

    public StudentDetails adddata(StudentDetails info)  {

        StudentDetails result= this.studentDetailsRepository.save(info);
        return result;

    }
    public List<StudentDetails> findAll(){
        List<StudentDetails> bes = (List<StudentDetails>)studentDetailsRepository.findAll();
        return bes;
    }
}
