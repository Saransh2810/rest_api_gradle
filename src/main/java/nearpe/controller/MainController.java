package nearpe.controller;

import nearpe.model.AreaInfo;
import nearpe.service.AreaInfoService;
import nearpe.exception.ResourceNotFoundException;
import nearpe.exception.ResourceAlreadyExistsException;
import nearpe.exception.BadResourceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/geo")
public class MainController {

    @Autowired
    AreaInfoService areaInfoService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostMapping("/adddata")
    public AreaInfo adddata(@RequestBody AreaInfo info)
    {

        AreaInfo b = this.areaInfoService.adddata(info);
        return b;
    }

    // update to create an district*/
    @RequestMapping(value = "/updating/{id}", method = RequestMethod.PUT)
    public AreaInfo updateIt(@RequestBody AreaInfo areaInfo,@PathVariable("id") long id) {
        areaInfoService.updateRecord(areaInfo,id);
        return areaInfo;
    }


    //get all districts/
   /* @GetMapping("/getall")
    public List<AreaInfo> getAll() {
        return areaInfoService.findAll();
    }
    @RequestMapping(value = "/task", method = RequestMethod.GET)
    public String taskList(Map<String, Object> model) {
        model.put("task", areaInfoService.findAll());
        return "hello";
    }*/
    @RequestMapping(value = "/task", method = RequestMethod.GET)
    public ModelAndView taskList(Map<String, Object> model) {

        ModelAndView modelAndView=new ModelAndView();

        modelAndView.addObject("task",areaInfoService.findAll().toString());
        modelAndView.setViewName("hello");
        return modelAndView;
    }



    //delete by id
    @RequestMapping(value = "/dely/{id}", method = RequestMethod.GET)
    void deleteById(@PathVariable long id) throws ResourceNotFoundException{
        areaInfoService.deleteById(id);
    }


    @GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AreaInfo> findById(@PathVariable long id) {
        try {
            AreaInfo info = areaInfoService.findById(id);

            return ResponseEntity.ok(info);  // return 200, with json body
        } catch (ResourceNotFoundException ex) {
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null); // return 404, with null body
        }
    }

}